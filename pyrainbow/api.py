import os
import mmap
import struct

from constants import G_STATUS_REG, MASKS

f = None
mem = None

MAPPED_SIZE = 4096
BASE_REGISTER = 0xFFA00000


def init():
    global f, mem
    f = os.open("/dev/mem", os.O_RDWR | os.O_SYNC)
    mem = mmap.mmap(f, MAPPED_SIZE, mmap.MAP_SHARED, mmap.PROT_READ | mmap.PROT_WRITE, offset=BASE_REGISTER)


def close():
    global f, mem
    mem.close()
    os.close(f)


def write(dest, value):
    mem.seek(dest)
    mem.write_byte(struct.pack("B", value))


def binmask(value):
    mask = 0x00
    mask |= MASKS['WAN'] if value & 0x01 else 0
    mask |= MASKS['LAN1'] if value & 0x02 else 0
    mask |= MASKS['LAN2'] if value & 0x04 else 0
    mask |= MASKS['LAN3'] if value & 0x08 else 0
    mask |= MASKS['LAN4'] if value & 0x10 else 0
    mask |= MASKS['LAN5'] if value & 0x20 else 0
    mask |= MASKS['WIFI'] if value & 0x40 else 0
    mask |= MASKS['PWR'] if value & 0x80 else 0
    write(G_STATUS_REG, ~mask & 0xFF)
