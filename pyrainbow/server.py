#!/usr/bin/env python
import socket
import struct

from api import init, binmask, close, write
from constants import OP_BINMASK, OP_RAW


class RainbowServer(object):
    def __init__(self, ip="", port=31337):
        self._s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._s.bind((ip, port))
        print "Listening on port %s" % port

    def run(self):
        init()
        while True:
            try:
                data, addr = self._s.recvfrom(1024)
                op, dest, val = struct.unpack("BBB", data)
                if op == OP_BINMASK:
                    binmask(val)
                    print "binmask {0:08b}".format(val)
                elif op == OP_RAW:
                    write(dest, val)
                    print "raw: dest = {0}, val = {1}".format(dest, val)
            except Exception:
                pass  # immortality FTW
            except KeyboardInterrupt:
                close()
                break

if __name__ == '__main__':
    server = RainbowServer()
    server.run()