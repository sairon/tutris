# C defines
COLORS = {
    'WAN_R': 0x13,
    'WAN_G': 0x14,
    'WAN_B': 0x15,
    'LAN_R': 0x16,
    'LAN_G': 0x17,
    'LAN_B': 0x18,
    'WIFI_R': 0x19,
    'WIFI_G': 0x1A,
    'WIFI_B': 0x1B,
    'PWR_R': 0x1C,
    'PWR_G': 0x1D,
    'PWR_B': 0x1E,
}

MASKS = {
    'WAN': 0x01,
    'LAN5': 0x02,
    'LAN4': 0x04,
    'LAN3': 0x08,
    'LAN2': 0x10,
    'LAN1': 0x20,
    'WIFI': 0x40,
    'PWR': 0x80,
}

OP_RAW = 1
OP_BINMASK = 2

G_OVERRIDE_REG = 0x22
G_STATUS_REG = 0x23

INTENSLVL_REG = 0x20

WAN_MASK = 0x01

WIFI_HW_STATUS_REG = 0x08