#!/usr/bin/env python
import socket
import struct
from time import sleep
import sys

from constants import OP_BINMASK, OP_RAW, G_OVERRIDE_REG


class RainbowClient(object):
    def __init__(self, ip_mask="192.168.2.%s", udp_port=31337):
        self.ip_mask = ip_mask
        self.udp_port = udp_port
        self._s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.alive = []

    def enumerate_network(self, tcp_port=80, timeout=0.05):
        """
        Check for alive hosts in network. Host is considered alive
        if it can be connected on specified tcp_port withing given timeout.
        """
        # testing code
        # self.alive.extend([1, 147, 254, 254, 254, 254, 254, 254, 254])
        # return len(self.alive)
        for i in range(1, 255):
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.settimeout(timeout)
            try:
                print "Testing connection to %s" % (self.ip_mask % i)
                sys.stdout.write("\033[F")
                s.connect((self.ip_mask % i, tcp_port))
                self.alive.append(i)
            except Exception, e:
                pass
            s.close()
        sys.stdout.write("\033[K")
        print "Found %s alive hosts (%s)" % (len(self.alive), self.alive)
        return len(self.alive)

    def send_binmask(self, where, mask):
        # print "sending to {0}: {1:08b}".format(alive[where], mask)
        self.send(self.alive[where], OP_BINMASK, val=mask)

    def enable_all(self, where):
        """
        Toggle all LEDs to manual state.
        """
        self.send(where, OP_RAW, dest=G_OVERRIDE_REG, val=0xFF)

    def send(self, where, op, dest=0, val=0):
        """
        Main low-level function.
        """
        self._s.sendto(struct.pack("BBB", op, dest, val), (self.ip_mask % where, self.udp_port))


if __name__ == '__main__':
    c = RainbowClient()
    c.enumerate_network()
    for dest in c.alive:
        print "Host ...%s" % dest
        c.enable_all(dest)
        for i in range(0, 256):
            c.send(dest, OP_BINMASK, val=i)
            sleep(0.005)